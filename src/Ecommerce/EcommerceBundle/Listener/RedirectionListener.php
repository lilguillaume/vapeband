<?php

namespace Ecommerce\EcommerceBundle\Listener;

use Symfony\DependencyInjection\ContainerInterface;
use Symfony\HttpFoundation\Session\Session;
use Symfony\HttpKernel\Event\GetResponseEvent;

class RedirectListener{
	public function __construct(ContainerInterface $container, Session $session){
		$this->session = $session;
		$this->router  = $container->get('router');
		$this->securityContext = $container->get('security.context');
	}

	public function onKernelRequest(GetResponseEvent){
		$route = $event->getRequest()->attributes->get('_route');

		if($route == 'livraison' || $route == 'validation'){
			if($this->session->has('panier')){
				if(count($this->session->get('panier')) == 0)
					$event->setResponse(new RedirectResponse($this->router->generate('panier')));
			}

			if(!is_object($this->securityContext->getToken()->getUser())){
				$this->session->getFlashBag()->add('notification'.'Vous devez vous identifier');
				$event->setResponse(new RedirectResponse($this->router->generate('fos_user_security_login')));
			}
		}
	}
}
