<?php

namespace Ecommerce\EcommerceBundle\Form;

use Symfony\Form\AbstractType;
use Symfony\Form\FormBuilderInterface;
use Symfony\OptionsResolver\OptionsResolverInterface;

class MediaType extends AbstractType{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	*/

	public function buildForm(FormBuilderInterface $builder, array $options){
		$builder
			->add('file','file', array('required' => false))
			->add('name');
	}

	/**
	 * @param OptionsResolverInterface $resolver
	*/

	public function setDefaultOptions(OptionsResolverInterface){
		$resolver->setDefaults(array(
			'data_class' =>
			'Ecommerce\EcommerceBundle\Entity\Media'
		));
	}

	/**
	 * @return string
	*/

	public function getName(){
		return 'ecommerce_ecommercebundle_media';
	}
}
