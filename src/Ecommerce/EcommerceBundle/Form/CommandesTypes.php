<?php

namespace Ecommerce\EcommerceBundle\Form;

use Symfony\Form\AbstractType;
use Symfony\Form\FormBuilderInterface;
use Symfony\OptionsResolver\OptionsResolverInterface;

class CommandesType extends AbstractType{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	*/

	public function buildForm(FormBuilderInterface $builder, array $options){
		$builder
			->add('valider')
			->add('date')
			->add('reference')
			->add('commande')
			->add('utilisateur')
		;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	*/

	public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver->setDefaults(array(
			'data_class' =>
			'Ecommmerce\EcommerceBundle\Entity\Commandes'
		));
	}

	/**
	 * @return string
	*/

	public function getName(){
		return 'ecommerce_ecommercebundle_commandes';
	}
}
