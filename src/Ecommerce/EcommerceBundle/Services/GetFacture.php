<?php

namespace Ecommerce\EcommerceBundle\Services;

use Symfony\Security\Core\SecurityContextInterface;
use SymfonyDependencyInjection\ContainerInterface;

class GetFacture{
	public function __construct(ContainerInterface $container){
		$this->container = $container;
	}

	public function facture($facture){
		$html = $this->container->get('templating')->render('UtilisateurBundle:Default:layout/facturePDF.html.twig', array('facture' => $facture));

		$html2pdf = new \Html2Pdf_Html2Pdf('P','A4','fr');
		$html2pdf->pdf->SetAuthor('Vapeband');
		$html2pdf->pdf->SetTitle('Facture' . $facture->getReference());
		$html2pdf->SetSubject('Facture Vapeband');
		$html2pdf->pdf->SetKeywords('facture,anouchkaTechShop');
		$html2pdf->SetDisplayMode('real');
		$html2pdf->writeHTML($html);
		$html2pdf->Output('Facture.pdf');

		$response = new Response();
		$response->headers->set('Content-type', 'application/pdf');

		return $response;
	}
}
