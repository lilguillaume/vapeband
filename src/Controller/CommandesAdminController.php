<?php
/**
 * Created by PhpStorm.
 * User: Guillaume PC
 * Date: 20/01/2019
 * Time: 08:00
 */
namespace Controller;

use Symfony\HttpFoundation\Requests;
use Symfony\FrameworkBundle\Controller;

use Ecommerce\EcommerceBundle\Entity\Commandes;
use Ecommerce\EcommerceBundle\Form\CommandesType;

/**
 * Commandes controller
 */
class CommandesAdminController extends Controller{
    public function commandesAction(){
        $em = $this->getDoctorine()->getManager();
        $commandes = $em->getRepository('Commandes')->findAll();

        return $this->render('Administration:Commandes/layout/index.html.twig', array('commandes' => $commandes));
    }

    public function showFactureAction($id){
        $em = $this->getDoctrine()->getManager();
        $facture = $em->getRepository('Commandes')->find($id);

        if(!$facture){
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenue');
            return $this->redirect($this->generateUrl('adminCommande'));
        }

        $this->container->get('setNewFacture')->facture($facture);
    }
}
