<?php
/**
 * Created by PhpStorm.
 * User: Guillaume PC
 * Date: 20/01/2019
 * Time: 07:47
 */
namespace Controller;

use Symfony\FrameworkBundle\Controller;

class CategoriesController extends Controller{
    public function menuAction(){
        $em = $this->getDoctorine()->getManager();
        $categories = $em->getRepository('Categories')->findAll();

        return $this->render('Default:categories/moduleUsed/menu.html.twig', array('categories' => $categories));
    }
}
