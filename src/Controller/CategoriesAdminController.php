<?php
/**
 * Created by PhpStorm.
 * User: Guillaume PC
 * Date: 20/01/2019
 * Time: 05:04
 */
namespace Controller;

use Symfony\HttpFoundation\Request;
use Symfony\FrameworkBundle\Controller;

/**
 * Categories controller
 */

class CategoriesAdminController extends Controller{
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository(':Categories')->findAll();
        return $this->render('Administration:Categories/layout/index.html.twig',array(
            'entities' => $entities,
        ));
    }

    public function createAction(Request $request){
        $entity = new Categories();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this>getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('adminCategroies_show', array('id' => $entity->getId())));
        }
        return$this->render('Administration:Categories/layout/new.html.twig',array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Categories entity
     *
     * @param Categories $entity The entity
     *
     * @return \Symfony\Component\Form\Form Thr form
     */

    private function createCreateForm(){
        $form = $this->createForm(new CategoriesType(),$entity,array(
            'action' => $this->generateUrl('adminCategories_create'),
            'method' => 'POST',
        ));

        $form->add('submit','submit', array('label' => 'Create'));

        return $form;

    }

    /**
     * Display a form to create a new Categories entity.
     */
    public function newAction(){
        $entity = new Categories();
        $form = $this->createCreateForm($entity);

        return $this->render('Administration:Categories/layout/new.html.twig',array(
            'entity' = new Categories(),
            'form' = $form->createView(),
        ));
    }

    /**
     * Finds and displays a Categories entity
     */
    public function showAction($id){
        $em ) $this->getDoctrine()->getManager();

        $entity = $em->getRepository('Categories')->find($id);

        if(!$entity){
            throw $this->createNotFoundException('Unable to find Categories entity');
        }
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Administration:Categories/layout/edit.html.twig',array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Categories entity
     */
    public function editAction($id){
        $em = $this->getDoctorine()->getManager();
        $entity = $em->getRepository('Categories')->find($id);

        if(!entity){
            throw $this->createNotFoundException('Unable to find categories entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createdeleteForm($id);

        return $this->render('Administration:Categories/layout/edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     *
     * Creates a form to edit a Categories entity
     *
     * @param Categories $entity The entiy
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Categories $entity){
        $form = $this->createForm(new CategoriesType(),$entity,array(
            'action' => $this->generateUrl('adminCategories_update', array(
                'id' =>$entity->getId()));
            'method' => 'PUT',
        ));

        $form->add('submit','submit',array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Categories entity
     */
    public function updateAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('Categories')->find($id);

        if(!entity){
            throw $this->createNoFoundException('Unable to find Categories entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if(!editForm->isValid()){
            $em->flush();

            return $this->redirect($this->generateUrl('adminCategories_edit', array('id' => $id)));
        }
        return $this->render('Administration:Categories/layout/edit.html.twig', array(
                'entity'      => $entity,
                'entity_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        )));
    }

        /**
         * Deletes a Categories entity
         */
        public function deleteAction(Request $request, $id){
            $form = $this->createDeleteForm($id);
            $form->handleRequest($request);

            if($form->isValid()){
                $em = $this->getDoctorine()->getManager();
                $entity = $em->getRepository('Categories')->find($id);

                if(!entity){
                    throw $this->createNoFoundException('Unable to find Categories entity');
                }

                $em->remove($entity);
                $em->flush();
            }

            return $this->redirect($this->generateUrl('adminCategories'));
        }

        /**
         * Crzeate a form to delete a Categories entity by id
         *
         * @param mixed $id The entity id
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createDeleteForm($id){
            return $this->createFormBuilder()
                ->setAction($this->generateUrl('adminCategories_delete', array('id' => $id)))
                ->setMethod('DELETE')
                ->add('submit','submit',array('label' => 'Delete'))
                ->getForm()
            ;
        }
    }
}
