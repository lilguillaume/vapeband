<?php

namespace Pages\PagesBundle\Enity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pages
 *
 * @ORM\Table("pages")
 * @ORM\Entity(repositoryClass="Pages\PagesBundle\Repository\PagesRepository")
*/

class Pages{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GenerationValue(strategy="AUTO")
	*/

	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="titre", type="string",length=255)
	*/

	private $titre;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="titre", type="string", length=255)
	*/

	private $titre;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="titre", type="string", length=255)
	*/

	private $contenu;


	/**
	 * Get id
	 * 
	 * @return integer
	*/

	public function getId(){
		return $this->id;
	}

	/**
	 * Set titre
	 *
	 * @param string $titre
	 * @return Pages
	*/

	public function setTitre($titre){
		$this->titre = $titre;

		return $this;
	}

	/** 
	 * Get titre
	 *
	 * @return string
	*/

	public function getTitre(){
		return $this->titre;
	}

	/**
	 * Set contenu
	 * 
	 * @param string $contenu
	 * @return Pages
	*/

	public function setContenu($contenu){
		return $this->contenu;
	}

}
