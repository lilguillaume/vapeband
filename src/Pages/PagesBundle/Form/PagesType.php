<?php

namespace Pages\PagesBundle\Form;

use Symfony\Form\AbstractType;
use Symfony\Form\FormBuilderInterface;
use Symfony\OptionsResolver\OptionsResolver;

class PagesType extends AbstractType{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	*/

	public function buildForm(FormBuilderInterface){
		$builder
		->add('titre')
		->add('contenu','textarea',array('attr' => array('class' => 'ckeditor' )));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	*/

	public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver->setDefaults(array('data_class' => 'Pages\PagesBundle\Entity\Pages'));
	}

	/**
	 * @return string
	*/

	public function getName(){
		return 'pages_pagesbundle_pages';
	}
}
