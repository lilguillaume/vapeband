<?php

namespace Pages\PagesBundle\DependencyInjection;

use Symfony\DependencyInjection\Container;
use Symfony\Config\FileLocator;
use Symfony\HttpKernel\DependencyInjection\Extension;
use Symfony\DependencyInjection\Loader;

/**
 * {@inheritDoc}
*/
class PagesExtension extends Extension {
	public function load(array $configs, ContainerBuilder $container){
	$configuration = new Configuration();
	$config = $this->processConfiguration($configuration, $configs);

	$loader = new Loader\YamFileLoader($container, new FileLocator(__DIR__.'/../Ressources/config'));
	$loader->load('services.yml');
	}
}
