<?php
/**
 * Created by PhpStorm.
 * User: Guillaume PC
 * Date: 02/02/2019
 * Time: 21:27
 */
namespace Ecommerce\EcommerceBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ecommerce\EcommerceBundle\Entity\Media;

class MediaData extends AbstractFixture implements OrderedFixtureInterface{
    public function load(ObjectManager $manager){
        $media1 = new Media();
        $media1  -> setPath('https://www.evaps.fr/documents/media/images/contenu/kit-mag-grip-smok-couleurs.jpg');
        $media1      -> setAlt('KIT MAG GRIP 100W - SMOKTECH');
        $manager -> persist($media1);

        $media2 = new Media();
        $media2 -> setPath('https://www.evaps.fr/documents/media/images/contenu/snowwolf-mfeng-200w-couleurs.jpg');
        $media2 -> setAlt('KIT MFENG SNOWWOLF 200W - SIGELEI');
        $manager-> persist($media2);

        $media3 = new Media();
        $media3 -> setPath('https://www.evaps.fr/documents/media/images/contenu/kit-istick-pico-x-eleaf-couleurs.jpg');
        $media3 -> setAlt('KIT ISTICK PICO X 75W - ELEAF');
        $manager->persist($media3);

        $media4 = new Media();
        $media4 -> setPath('https://www.evaps.fr/documents/media/images/contenu/kit-p16a-justfog02.jpg');
        $media4 -> setAlt('KIT P16A - JUSTFOG');
        $manager->persist($media4);

        $media5 = new Media();
        $media5 -> setPath('https://www.evaps.fr/documents/media/images/contenu/kit-luxotic-wismec-couleur.jpg');
        $media5 -> setAlt('KIT LUXOTIC DF - WISMEC');
        $manager->persist($media5);

        $media6 = new Media();
        $media6 -> setPath('https://www.evaps.fr/documents/media/images/contenu/species-smoktech04.jpg');
        $media6 -> setAlt('KIT SPECIES 230W - SMOKTECH');
        $manager->persist($media6);

        $media7 = new Media();
        $media7 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-box-luxe-vaporesso-couleurs.jpg');
        $media7 -> setAlt('Kit Luxe / Skrr - Vaporesso');
        $manager-> persist($media7);

        $media8 = new Media();
        $media8 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-aegis-mini-geekvape.jpg');
        $media8 -> setAlt('Kit Aegis Mini - Geekvape');
        $manager->persist($media8);

        $media9 = new Media();
        $media9 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-istick-amnis-eleaf.jpg');
        $media9 -> setAlt('Kit Istick Amnis - Eleaf');
        $manager -> persist($media8);

        $media10 = new Media();
        $media10 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-active-wismec.jpg');
        $media10 -> setAlt('Kit Active 80 - Wismec');
        $manager ->persist($media10);

        $media11 = new Media();
        $media11 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-pod-novo.jpg');
        $media11 -> setAlt('Pod Novo - Smoktech');
        $manager -> persist($media11);

        $media12 = new Media();
        $media12 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-diamond-vpc02.jpg');
        $media12 -> setAlt('Kit Diamond VPC - iJoy');
        $manager ->persist($media12);

        $media13 = new Media();
        $media13 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-tarot-baby-vaporesso-couleurs.jpg');
        $media13 -> setAlt('Kit Tarot Baby - Vaporesso');
        $manager -> persist($media13);

        $media14 = new Media();
        $media14 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-zenith-3-vv-ijoy03.jpg');
        $media14 -> setAlt('Kit Zenith 3 VV - Ijoy');
        $manager -> persist($media14);

        $media15 = new Media();
        $media15 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kit-smok-i-priv04.jpg');
        $media15 -> setAlt('Kit I-Priv 230W - Smoktech');
        $manager -> persist($media15);

        $media16 = new Media();
        $media16 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/5-56-gorilla-warfare.jpg');
        $media16 -> setAlt('5.56 Blue Sweet Agave Cactus - Gorilla Warfare');
        $manager -> persist($media16);

        $media17 = new Media();
        $media17 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/50-gorilla-warfare.jpg');
        $media17 -> setAlt('50 Pineapple Dragon Fruit - Gorilla Warfare');
        $manager -> persist($media17);

        $media18 = new Media();
        $media18 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/270-reloaded-gorilla-warfare.jpg');
        $media18 -> setAlt('270 Reloaded Frosted Pastry - Gorilla Warfare');
        $manager -> persist($media18);

        $media19 = new Media();
        $media19 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/jus-de-boudin-blanc-le-french-liquide.jpg');
        $media19 -> setAlt('Jus De Boudin Blanc - Le French Liquide');
        $manager -> persist($media19);

        $media20 = new Media();
        $media20 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/vapeflam-hi2.jpg');
        $media20 -> setAlt('Hi2 - Vape Flam');
        $manager -> persist($media20);

        $media21 = new Media();
        $media21 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/reserve-1850-ben-northon.jpg');
        $media21 -> setAlt('Réserve 1850 50ml - Ben Northon');
        $manager -> persist($media21);

        $media22 = new Media();
        $media22 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/classic-n1-marie-jeanne.jpg');
        $media22 -> setAlt('Classic N°1 CBD - Marie Jeanne');
        $manager -> persist($media22);

        $media23 = new Media();
        $media23 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/framboise-fruits-by-fuu.jpg');
        $media23 -> setAlt('Framboise - Fruuits by Fuu');
        $manager -> persist($media23);

        $media24 = new Media();
        $media24 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/citron-fruits-by-fuu.jpg');
        $media24 -> setAlt('Citron - Fruuits by Fuu');
        $manager -> persist($media24);

        $media25 = new Media();
        $media25 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/ananas-fruits-by-fuu.jpg');
        $media25 -> setAlt('Ananas - Fruits by Fuu');
        $manager -> persist($media25);

        $media26 = new Media();
        $media26 ->  setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/pomme-fruuits-by-fuu.jpg');
        $media26 -> setAlt('Pomme - Fruuits by Fuu');
        $manager -> persist($media26);

        $media27 = new Media();
        $media27 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/fraise-fruits-by-fuu.jpg');
        $media27 -> setAlt('Fraise - Fruuits by Fuu');
        $manager -> persist($media27);

        $media28 = new Media();
        $media28 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/badiane-rouge-minimal.jpg');
        $media28 -> setAlt('Badiane Rouge - Minimal');
        $manager -> persist($media28);

        $media29 = new Media();
        $media29 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/bubble-gum-10ml-bee.jpg');
        $media29 -> setAlt('Bubblegum 10ml - Bee');
        $manager -> persist($media29);

        $media30 = new Media();
        $media30 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/fkc-10ml-bee.jpg');
        $media30 -> setAlt('FKC 10ml - Bee');
        $manager -> persist($media30);

        $media31 = new Media();
        $media31 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/melony-%C2%B2-bee-10ml.jpg');
        $media31 -> setAlt('Melony² 10ml - Bee');
        $manager -> persist($media31);

        $media32 = new Media();
        $media32 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/fraisio-10ml.jpg');
        $media32 -> setAlt('Fraisio 10ml - Bee');
        $manager -> persist($media32 );

        $media33 = new Media();
        $media33 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/frutty-red-10-ml-bee.jpg');
        $media33 -> setAlt('Frutty Red 10ml - Bee');
        $manager -> persist($media33);

        $media34 = new Media();
        $media34 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/citrius-10ml-bee.jpg');
        $media34 -> setAlt('Citrius 10ml - Bee');
        $manager -> persist($media34);

        $media35 = new Media();
        $media35 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/bio-france-les-fruits-de-m-li.jpg');
        $media35 -> setAlt('Les Fruits de M. Li French Malaisien - Bio France');
        $manager -> persist($media35);

        $media36 = new Media();
        $media36 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/bio-france-or-d-asie.jpg');
        $media36 -> setAlt('Or d\'Asie French Malaisien - Bio France');
        $manager -> persist($media36);

        $media37 = new Media();
        $media37 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/dragon-rouge.jpg');
        $media37 -> setAlt('Dragon Rouge French Malaisien - Bio France');
        $manager -> persist($media37);

        $media38 = new Media();
        $media38 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/bio-france-paris-pekin.jpg');
        $media38 -> setAlt('Paris Pekin French Malaisien - Bio France');
        $manager -> persist($media38);

        $media39 = new Media();
        $media39 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/bio-france-sur-le-mekong.jpg');
        $media39 -> setAlt('Sur le Mékong French Malaisien - Bio France');
        $manager -> persist($media39);

        $media40 = new Media();
        $media40 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/cloud-vapor-punch-berries.jpg');
        $media40 -> setAlt('Punch Berries 10ml - Cloud Vapor');
        $manager -> persist($media40);

        $media41 = new Media();
        $media41 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/cloud-vapor-red-leaf.jpg');
        $media41 -> setAlt('Red Leaf 10ml - Cloud Vapor');
        $manager -> persist($media41);

        $media42 = new Media();
        $media42 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/creme-brulee-jtjm.jpg');
        $media42 -> setAlt('Crème brûlée 50ml - Jimmy The Juice Man');
        $manager -> persist($media42);

        $media43 = new Media();
        $media43 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/vaporigins-blood-citrus.jpg');
        $media43 -> setAlt('Blood Citrus 80ml - Vaporigins');
        $manager -> persist($media43);

        $media44 = new Media();
        $media44 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/kingside-tobacco-five-pawns.jpg');
        $media44 -> setAlt('Kingside Tobacco 50ml - Five Pawns');
        $manager -> persist($media44);

        $media45 -> new Media();
        $media45 -> setPath('https://www.evaps.fr/documents/media/images/contenu/miniature/royal-tobacco-five-pawns.jpg');
        $media45 -> setAlt('Royal Tobacco 50ml - Five Pawns');
        $manager -> persist($media45);

        $media46 -> new Media
    }
}