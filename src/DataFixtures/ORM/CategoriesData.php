<?php
/**
 * Created by PhpStorm.
 * User: Guillaume PC
 * Date: 02/02/2019
 * Time: 20:53
 */
namespace Ecommerce\EcommerceBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Ecommerce\EcommerceBundle\Entity\Categories;

class CategoriesData extends AbstractFixture implements OrderedFixtureInterface{
    public function load(ObjectManager $manager){
        $categorie1 = new Categories();
        $categorie1->setNom('E-cigarettes');
        $categorie1->setImage($this->getReference('media1'));
        $manager->persist($categorie1);

        $categorie2 = new Categories();
        $categorie2->setNom('E-liquides');
        $manager->persist($categorie2);

        $categorie3 = new Categories();
        $categorie3->setNom('DIY');
        $manager->persist($categorie3);

        $manager->flush();

        $this->addReference('categorie1',$categorie1);
        $this->addReference('categorie2',$categorie2);
        $this->addReference('categorie3',$categorie3);
    }

    public function getOrder(){
        return 2;
    }
}
